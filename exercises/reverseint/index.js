// --- Directions
// Given an integer, return an integer that is the reverse
// ordering of numbers.
// --- Examples
//   reverseInt(15) === 51
//   reverseInt(981) === 189
//   reverseInt(500) === 5
//   reverseInt(-15) === -51
//   reverseInt(-90) === -9

/** End result should be a number */

const reverseInt1 = n => {
  let revInt = 0,
    mult = 1,
    splicedNumber = n;
  while (splicedNumber > 0) {
    splicedNumber = Math.trunc(splicedNumber / 10);
    revInt += (splicedNumber % 10) * (10 * mult)
    mult++
  }
  console.log(revInt);
  return revInt;
}

const reverseInt2 = n => Math.abs(n).toString().split('').reverse().join('') * Math.sign(n);

const reverseInt3 = n => {
  if (!n) return 0;
  const sign = Math.sign(n),
    digits = [];
  let reducedNumber = Math.abs(n);
  while (reducedNumber > 0) {
    digits.unshift(reducedNumber % 10);
    reducedNumber = Math.trunc(reducedNumber / 10);
  }
  return sign * digits.reduce((acc, val, i) => acc + (val * 10 ** i))
}

module.exports = reverseInt2;