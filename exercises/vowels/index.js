// --- Directions
// Write a function that returns the number of vowels
// used in a string.  Vowels are the characters 'a', 'e'
// 'i', 'o', and 'u'.
// --- Examples
//   vowels('Hi There!') --> 3
//   vowels('Why do you ask?') --> 4
//   vowels('Why?') --> 0

const letters = ['a', 'e', 'i', 'o', 'u'];
/** My simple solution */
const vowels = str => {
  let chars = 0;
  for (const char of str.toLowerCase()) {
    if (letters.indexOf(char) > -1) chars++;
  }
  console.log(chars);
  return chars;
}

/** My solution using regex 
 * g is global i is insensitive to casing in the string
 * test method of regex was learned from Max.
 */
const vowels2 = str => {
  let chars = 0
  for (const char of str) {
    if (/[aeiou]/gi.test(char)) chars++;
  }
  return chars;
}

/** Stephen solution with a string and includes
 */
const vowels3 = str => {
  let count = 0;
  // const checker = [] the same array i declared at the top
  for (const char of str.toLowerCase()) {
    if (letters.includes(char)) count++;
  }
  return count;
}

/** Stephen solution with a regex
 * He uses a native string match function and checks the group
 * of vowels with the gi flags, this method returns an array with
 * the matches or null if none was found, thats why we need to
 * check both possible outcomes, we cannot return just the length
 */
const vowels4 = str => {
  const matches = str.match(/[aeiou]/gi)
  return matches ? matches.length : 0;
}

/** Takeouts
 * str.match(regex) returns an array with the matched strings
 * regex.text(str) return true if str matches
 */

module.exports = vowels4;