// --- Directions
// Check to see if two provided strings are anagrams of eachother.
// One string is an anagram of another if it uses the same characters
// in the same quantity. Only consider characters, not spaces
// or punctuation.  Consider capital letters to be the same as lower case
// --- Examples
//   anagrams('rail safety', 'fairy tales') --> True
//   anagrams('RAIL! SAFETY!', 'fairy tales') --> True
//   anagrams('Hi there', 'Bye there') --> False

/** My Helper function to build character maps */
const mapCharacters = str => {
	const charMap = {};
	let charHelper = '';
	for (const char of str) {
		charHelper = char.toLowerCase();
		if (/[a-z]/.test(charHelper)) {
			charMap[charHelper] = charMap[charHelper] + 1 || 1;
		}
	}
	return charMap;
};

/** Stephen helper function converting the string with
 * regex at first
 */
const buildCharMap = str => {
	const charMap = {};
	/** Regex to remove all not [w] characters from a string */
	for (const char of str.replace(/[^\w]/g, '').toLowerCase()) {
		charMap[char] = charMap[char] + 1 || 1;
	}
	return charMap;
};

/** Check object deep equality. Things I did not know
 * Object.keys returns an array with strings of the keys of an object
 */
const isEquivalent = (a, b) => {
	if (Object.keys(a).length !== Object.keys(b).length) {
		return false;
	}
	for (const key in a) {
		if (a[key] !== b[key]) {
			return false;
		}
	}
	return true;
};

const anagrams = (stringA, stringB) => {
	return isEquivalent(mapCharacters(stringA), mapCharacters(stringB));
};

const anagrams2 = (stringA, stringB) => {
	const charMapA = buildCharMap(stringA);
	const charMapB = buildCharMap(stringB);
	/** Checking for object equality stephen methods 
   * First check if number of keys are equal, if not retur false
   */
	if (Object.keys(charMapA).length !== Object.keys(charMapB).length) {
		return false;
	}
	/** Iterate over any object to check for value equality
   * General reminder here. FOR IN is used to iterates objects
   * it should not be used to iterate arrays because on each iteration
   * it returns the object KEY, so at keast i can mark my head with the
   * difference
   * FOR IN: returns keys
   * FOR OF: returns the value of the key
   * 
   * Using forin in an array will return the index, so it is not completely
   * wrong if i need the index, and just getting the values will be arr[ index]
   */
	for (const key in charMapA) {
		if (charMapA[key] !== charMapB[key]) {
			return false;
		}
	}
	return true;
};

/** Performance concerns about the several iterations of the first solution.
 * Second way of resolving anagrams. I did not came up with this,
 * this uses an array. This uses the array native funcion sort. This uses a helper function
 * that strips out and lowercases the string and sort it by alphabetical order,
 * Then the anagram function just returns the 
 */
const cleanString = str => str.replace(/[^\w]/g, '').toLowerCase().split('').sort().join('');
const anagrams3 = (strA, strB) => cleanString(strA) === cleanString(strB);

module.exports = anagrams3;
