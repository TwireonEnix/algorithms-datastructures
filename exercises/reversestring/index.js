// --- Directions
// Given a string, return a new string with the reversed
// order of characters
// --- Examples
//   reverse('apple') === 'leppa'
//   reverse('hello') === 'olleh'
//   reverse('Greetings!') === '!sgniteerG'

/** Solution 1 
 * Using a Array.prototype.reverse: A ES6 built in function to flip
 * an array
 * flowchart -> convert string into array (split) -> reverse the array -> rejoin into string
 */
const reverse1 = str => str.split('').reverse().join('');

/** Solution 2
 * Without reversing
 */
const reverse2 = str => {
  const splitted = str.split('');
  let reversed = [];
  for (const i of splitted) {
    reversed.unshift(i);
  }
  return reversed.join('');
};

/** Solution 3
 * Without using arrays: 
 * Create an empty string, for each character in the parameter take the character
 * and add it to the start of reversed
 */
const reverse3 = str => {
  let reversed = '';
  for (let i = str.length; i >= 0; i--) {
    reversed += str.charAt(i)
  }
  return reversed;
}

/** Solution 4
 * For loops without arrays
 */
const reverse4 = str => {
  let reversed = '';
  for (const char of str) {
    reversed = char + reversed;
  }
  return reversed;
}

/** Solution 5
 * Using reduce helper: reduce takes all values on a given array and 
 * condense down to one single value
 * Usage reduce((condensedValue, currentValue) => fn, initialcondensedValue)
 */
const reverse5 = str => str.split('').reduce((rev, char) => char + rev, '');

module.exports = reverse5;

/** Stephen side notes: 
 * Wherever possible avoid classic for loops, however we cannot use them if the
 * lookup is secuential
 */