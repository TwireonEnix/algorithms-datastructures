// --- Directions
// Write a function that accepts a positive number N.
// The function should console log a pyramid shape
// with N levels using the # character.  Make sure the
// pyramid has spaces on both the left *and* right hand sides
// --- Examples
//   pyramid(1)
//       '#'
//   pyramid(2)
//       ' # '
//       '###'
//   pyramid(3)
//       '  #  '
//       ' ### '
//       '#####'

/**Stephen solution, i couldn't figure out how to decide whether or
 * not to put a # in the column. Shame on me.
 * 
 * To figure out number of columns, is how the n relates to the columns
 * The pattern was finding a midpoint and 
 */
const pyramid = n => {
  const midpoint = Math.floor((n * 2 - 1) / 2);
  for (let r = 0; r < n; r++) {
    let level = '';
    for (let c = 0; c < n * 2 - 1; c++) {
      midpoint - r <= c && midpoint + r >= c ?
        (level += '#') :
        (level += ' ');
    }
    console.log(level);
  }
};

const recursivePyramid = (n, row = 0, level = '') => {
  if (n === row) return;
  if (level.length === n * 2 - 1) {
    console.log(level);
    return recursivePyramid(n, row + 1);
  }
  const midpoint = Math.floor((n * 2 - 1) / 2);
  midpoint - row <= level.length && midpoint + row >= level.length ?
    level += '#' : level += ' ';
  recursivePyramid(n, row, level);
}

module.exports = recursivePyramid;