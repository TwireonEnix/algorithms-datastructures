// --- Directions
// Write a function that accepts a positive number N.
// The function should console log a step shape
// with N levels using the # character.  Make sure the
// step has spaces on the right hand side!
// --- Examples
//   steps(2)
//       '# '
//       '##'
//   steps(3)
//       '#  '
//       '## '
//       '###'
//   steps(4)
//       '#   '
//       '##  '
//       '### '
//       '####'

/** My algoritm, somewhat weird, but i haven't seen stephen
 * recommendations. This is an iterative solution.
 * The alternative is recursion
 * 
 */
const steps = n => {
  if (!n) return;
  let str = '';
  for (let i = 1; i <= Math.abs(n); i++) {
    for (let j = 1; j <= i; j++) {
      str += `#`;
    }
    for (let j = Math.abs(n); j >= i + 1; j--) {
      str += ` `;
    }
    console.log(str);
    str = '';
  }
};

/** Stephen solution
 * From 0 to n 
 * create empty string array,
 * from 0 to n if currect column is equal or les than current row
 * add # else add ' '
 * console log string
 * 
 * He highly recommeds starting any algorithm with pseudocode
 * 
 */

const steps2 = n => {
  n = n ? Math.abs(n) : 1;
  for (let i = 0; i < n; i++) {
    let stair = '';
    for (let j = 0; j < n; j++) {
      j <= i ? (stair += '#') :
        (stair += ' ');
    }
    console.log(stair);
  }
};

/** Recursion version And general tips for recursion
 * With recursion we always start with one type of code.
 * tips: 
 *  · Figure out the bare minimum pieces of information to represent
 *  your problem
 *  · Give reasonable defaults to the bare minimum pieces of info
 *  · Check the base case. Is there any work left to do? If not, return
 *  · Do some work. Call your funtion again, making sure the args have
 *  changed in some fashion
 * 
 * STEPS PROBLEM:
 * Identiying the general assumptions: 
 * · if (row === n) then we hit end
 * · if (stair.length === n) end of a row
 *  ·if stair.length <= row in that instance, add #, else add ' '
 */
const recursiveSteps = (n, row = 0, stair = '') => {
  if (n === row) return;
  if (n === stair.length) {
    console.log(stair);
    return recursiveSteps(n, row + 1);
  }
  stair.length <= row ? stair += '#' : stair += ' ';
  recursiveSteps(n, row, stair);
}
/** With string.repeat */
const steps3 = n => {
  for (let i = 1; i <= n; i++) console.log('#'.repeat(i) + ' '.repeat(n - i));
}

module.exports = steps3;