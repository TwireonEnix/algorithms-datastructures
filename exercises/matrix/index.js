// --- Directions
// Write a function that accepts an integer N
// and returns a NxN spiral matrix.
// --- Examples
//   matrix(2)
//     [[1, 2],
//     [4, 3]]
//   matrix(3)
//     [[1, 2, 3],
//     [8, 9, 4],
//     [7, 6, 5]]
//  matrix(4)
//     [[1,   2,  3, 4],
//     [12, 13, 14, 5],
//     [11, 16, 15, 6],
//     [10,  9,  8, 7]]

/**
 * Algorithm: This is constructed around the base of saying that there is a starting row, and
 * starting colum
 * · Create en empty array called 'results'
 * · Create a counter variable, starting at one
 * · As long as start clumn <= end column.
 *   · Loop from start column to end column
 *     · At results[start_row][i] assign counter variable
 *     · Increment counter
 *   · Increment start row
 *   · Loop from start row to end row
 *     · At results[i][end_column] assign counter variable
 *     · Increment counter
 *   · Decrement end roww
 *   · ...repeat for the other two sides  
 */
const matrix = n => {
	const results = [];
	/** Filling the array of subarrays
   * An important note on arrays is that if we push any value o any position, the
   * previus positions will have null values
   * arr[3] = 1;
   * -> [null, null, null, 1]
   */
	for (let i = 0; i < n; i++) {
		results.push([]);
	}
	let counter = 1,
		startColumm = 0,
		startRow = 0,
		endColumn = n - 1,
		endRow = n - 1;
	while (startColumm <= endColumn && startRow <= endRow) {
		// Top Row
		for (let i = startColumm; i <= endColumn; i++) {
			results[startRow][i] = counter;
			counter++;
		}
		startRow++;

		// Right column
		for (let i = startRow; i <= endRow; i++) {
			results[i][endColumn] = counter;
			counter++;
		}
		endColumn--;

		// Bottom Row
		for (let i = endColumn; i >= startColumm; i--) {
			results[endRow][i] = counter;
			counter++;
		}
		endRow--;

		// Left row
		for (let i = endRow; i >= startRow; i--) {
			results[i][startColumm] = counter;
			counter++;
		}
		startColumm++;
	}
	return results;
};

/** I had some troubles to advance from here, it was tough to understand this problem
 * It was interesting
 */

module.exports = matrix;
