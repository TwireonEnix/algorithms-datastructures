// --- Directions
// Given an array and chunk size, divide the array into many subarrays
// where each subarray is of length size
// --- Examples
// chunk([1, 2, 3, 4], 2) --> [[ 1, 2], [3, 4]]
// chunk([1, 2, 3, 4, 5], 2) --> [[ 1, 2], [3, 4], [5]]
// chunk([1, 2, 3, 4, 5, 6, 7, 8], 3) --> [[ 1, 2, 3], [4, 5, 6], [7, 8]]
// chunk([1, 2, 3, 4, 5], 4) --> [[ 1, 2, 3, 4], [5]]
// chunk([1, 2, 3, 4, 5], 10) --> [[ 1, 2, 3, 4, 5]]

/** My solution, this works. However, 
 * the first implementation violates the functional programming
 * principle, as this has the secondary effect of modyfing the original array
 * That's why i create a copy before iterating over it while splicing it.
 * Stephen came up with a similar solution but uses slice which does not remove
 * the elements in the array
 */
const chunk = (array, size) => {
  let index = 0;
  const aCopy = [...array];
  const chunked = [];
  while (index < array.length) {
    chunked.push(aCopy.splice(0, size));
    index += size;
  }
  return chunked;
};

/** Stephen solution one:
 * He declares a secuential algorithm with defined steps:
 * 1 Create empty array to hold chunks called 'chunked'
 * 2 For each element in then unchunked array
 * 3 Retrieve the last element in 'chunked'
 * 4 IF last element does not exist or if its length is equal
 * to chunk size
 * 5 Push a new chunck into 'chunked' with the current element
 * 6 ELSE add the current element into the chunk
 * 
 * To the bottom line I found this solution to be a little more complex than the one I came up with
 * However this does not use array helpers. Which is its advantage 
 */

const chunk2 = (array, size) => {
  const chunked = [];
  for (const element of array) {
    const last = chunked[chunked.length - 1];
    (!last || last.length === size) ? chunked.push([element]): last.push(element);
  }
  return chunked;
}

/** My version using slice */
const chunk3 = (array, size) => {
  let index = 0;
  const chunked = [];
  while (index < array.length) {
    chunked.push(array.slice(index, size + index));
    index += size;
  }
  return chunked;
}

/** Stephen version of slice method:
 * · Create empty chunked array
 * · Create index start at zero
 * · While index is less than array.length ->
 *  · Push a slice of length 'size' from 'array' into 'chuncked'
 *  · Add 'size' to 'index'
 * 
 * Stephen said that this solution is harder to come up with, but it is what i came up with in the first place.
 * However, I did not know the difference between slice and splice, that's something I learned today.
 * Splice alters the original array, while slice creates copies of it.
 * 
 * My version is equal than his in chunk3
 */

module.exports = chunk3;