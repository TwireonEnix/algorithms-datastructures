// --- Directions
// Given a string, return the character that is most
// commonly used in the string.
// --- Examples
// maxChar("abcccccccd") === "c"
// maxChar("apple 1231111") === "1"

/** My solution that passes all tests
 * What i learned here: how to access keys, properties
 * and values inside an object.
 */
const maxChar = str => {
  const charMap = {};
  let higherKey, max = 0;
  for (const char of str) {
    charMap.hasOwnProperty(char) ? charMap[char]++ : charMap[char] = 1;
  }
  for (const key in charMap) {
    if (max < charMap[key]) {
      higherKey = key;
    }
    max = charMap[key];
  }
  return higherKey;
}

/** Stephen way changing the object filling syntax by using boolean asignation
 * In general the final way is almost identical to mine. This is important when
 * asked about counting chars within a string
 */
const maxChar2 = str => {
  const chars = {};
  let max = 0,
    maxChar;
  for (const char of str) {
    chars[char] = chars[char] + 1 || 1;
  }
  for (const char in chars) {
    if (chars[char] > max) {
      max = chars[char];
      maxChar = char
    }
  }
  return maxChar;
};

module.exports = maxChar;