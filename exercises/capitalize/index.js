// --- Directions
// Write a function that accepts a string.  The function should
// capitalize the first letter of each word in the string then
// return the capitalized string.
// --- Examples
//   capitalize('a short sentence') --> 'A Short Sentence'
//   capitalize('a lazy fox') --> 'A Lazy Fox'
//   capitalize('look, it is working!') --> 'Look, It Is Working!'

/** My awkward solution 
 * I split the string in an array of letters to iterate over it,
 * Capitalize the first letter and concatenating the rest, then I
 * trim it to remove extra spaces
 */
const capitalize = str => {
  let capitalizedString = '';
  const splittedWords = str.split(' ');
  for (const word of splittedWords) {
    capitalizedString += `${word[0].toUpperCase()}${word.slice(1)} `
  }
  return capitalizedString.trim();
}

/** My solution number two
 * Check each char, if it's the beginning of the string or 
 * if the char is preceded by an empty space then capitalize
 * the letter, otherwise concatenate it
 * 
 */
const capitalize2 = str => {
  let capitalizedString = ''
  for (const char of str) {
    !capitalizedString || capitalizedString.endsWith(' ') ?
      capitalizedString += char.toUpperCase() :
      capitalizedString += char;
  }
  return capitalizedString;
}

/** Stephen first algorithm
 * · Make an empty array 'words'
 * · Split the input by spaces to get an array 
 * · For each word in the array ->
 *  · Uppercase the first letter of the word
 *  · Join the first letter with the rest of the word
 *  · Push result into 'words array
 * · Join words into a string an return
 * 
 * Fairly similar of what I did in the first solution,
 * however this solution is more elegant and compact
 */

const capitalize3 = str => {
  const words = [];
  for (const word of str.split(' ')) {
    words.push(word[0].toUpperCase() + word.slice(1))
  }
  return words.join(' ');
}

/** The stephen's second algoithm is exactly the same as
 * my second, however, he doesn't use nullity of the result
 * to capitalize the first one and ends making a for that starts
 * at 1, not that elegant
 * 
 */
module.exports = capitalize3;