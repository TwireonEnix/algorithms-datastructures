// --- Directions
// Given a string, return true if the string is a palindrome
// or false if it is not.  Palindromes are strings that
// form the same word if it is reversed. *Do* include spaces
// and punctuation in determining if the string is a palindrome.
// --- Examples:
//   palindrome("abba") === true
//   palindrome("abcdefg") === false

const palindrome1 = str => {
  const firstHalf = str.substring(0, str.length / 2)
  const secondReversedHalf = str.substring(str.length / 2, str.length).split('').reverse().join('');
  if (firstHalf.length !== secondReversedHalf.length) {
    return firstHalf === secondReversedHalf.substring(0, secondReversedHalf.length - 1)
  }
  return firstHalf === secondReversedHalf;
}

/** Easiest way if we're not comparing halfs, I forgot the base definition of palindrome, 
 * Palindroms are string that are equal if reversed. (WHOLE)
 */
const palindrome2 = str => str.split('').reverse().join('') === str;

/** Alternative
 * Using array helper called every() tests wheter all the elements in the array pass the test implemented
 * by the provided function, in the end this makes an and operation on the results, meaning that if one eval is false
 * the whole method will be false.
 * 
 * ex arr = [1, 2, 3]
 * arr.every(v => v < 5) // true
 * Not ideal, but we need to find more than one solution to a problem, even if it is not the optimal one, but
 * it open up the way for more innovative ways of thinking
 */
const palindrome3 = str => str.split('').every((char, i) => char === str[str.length - i - 1])

module.exports = palindrome3;