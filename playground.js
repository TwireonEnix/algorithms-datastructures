const count = n => {
  if (!n) return;
  console.log(n);
  return count(n - 1);
}

count(12)